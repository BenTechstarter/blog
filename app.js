// App.js
import React, { useState } from 'react';
import { BrowserRouter as Router, Route, Link, Switch } from 'react-router-dom';

function App() {
  const [posts, setPosts] = useState([]);

  const addPost = (title, content) => {
    setPosts([...posts, { id: posts.length + 1, title, content }]);
  };

  const deletePost = (id) => {
    setPosts(posts.filter(post => post.id !== id));
  };

  return (
    <Router>
      <div>
        <nav>
          <ul>
            <li>
              <Link to="/">Home</Link>
            </li>
            <li>
              <Link to="/create">Create Post</Link>
            </li>
          </ul>
        </nav>

        <Switch>
          <Route path="/create">
            <CreatePostForm addPost={addPost} />
          </Route>
          <Route path="/edit/:id">
            <EditPostForm posts={posts} setPosts={setPosts} />
          </Route>
          <Route path="/">
            <h2>Blog Posts</h2>
            <PostsList posts={posts} deletePost={deletePost} />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

function PostsList({ posts, deletePost }) {
  return (
    <div>
      {posts.map(post => (
        <div key={post.id}>
          <h3>{post.title}</h3>
          <p>{post.content}</p>
          <button onClick={() => deletePost(post.id)}>Delete</button>
          <Link to={`/edit/${post.id}`}>Edit</Link>
        </div>
      ))}
    </div>
  );
}

function CreatePostForm({ addPost }) {
  const [title, setTitle] = useState('');
  const [content, setContent] = useState('');

  const handleSubmit = (e) => {
    e.preventDefault();
    addPost(title, content);
    setTitle('');
    setContent('');
  };

  return (
    <form onSubmit={handleSubmit}>
      <input type="text" placeholder="Title" value={title} onChange={(e) => setTitle(e.target.value)} />
      <textarea placeholder="Content" value={content} onChange={(e) => setContent(e.target.value)} />
      <button type="submit">Submit</button>
    </form>
  );
}

function EditPostForm({ posts, setPosts }) {
  const postId = parseInt(window.location.pathname.split('/').pop());
  const post = posts.find(post => post.id === postId);

  const [title, setTitle] = useState(post ? post.title : '');
  const [content, setContent] = useState(post ? post.content : '');

  const handleSubmit = (e) => {
    e.preventDefault();
    setPosts(posts.map(p => {
      if (p.id === postId) {
        return { ...p, title, content };
      }
      return p;
    }));
    window.location.href = '/';
  };

  return (
    <form onSubmit={handleSubmit}>
      <input type="text" placeholder="Title" value={title} onChange={(e) => setTitle(e.target.value)} />
      <textarea placeholder="Content" value={content} onChange={(e) => setContent(e.target.value)} />
      <button type="submit">Submit</button>
    </form>
  );
}

export default App;
